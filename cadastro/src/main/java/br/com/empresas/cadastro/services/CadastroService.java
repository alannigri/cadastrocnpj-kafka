package br.com.empresas.cadastro.services;

import br.com.empresas.cadastro.models.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CadastroService {

    @Autowired
    private CadastroProducer cadastroProducer;

    public Empresa cadastrarEmpresa (Empresa empresa){

        cadastroProducer.enviarAoKafka(empresa);
        return empresa;
    }
}
