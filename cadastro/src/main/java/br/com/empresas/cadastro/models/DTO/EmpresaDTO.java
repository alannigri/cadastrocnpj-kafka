package br.com.empresas.cadastro.models.DTO;

import org.hibernate.validator.constraints.br.CNPJ;

public class EmpresaDTO {

    @CNPJ
    private String cnpj;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
}
