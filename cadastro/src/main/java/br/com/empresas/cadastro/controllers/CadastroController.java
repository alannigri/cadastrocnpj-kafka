package br.com.empresas.cadastro.controllers;

import br.com.empresas.cadastro.models.DTO.EmpresaDTO;
import br.com.empresas.cadastro.models.Empresa;
import br.com.empresas.cadastro.services.CadastroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cadastro")
public class CadastroController {

    @Autowired
    private CadastroService cadastroService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Empresa cadastrarEmpresa(@RequestBody @Valid EmpresaDTO empresaDTO) {
        Empresa empresa = new Empresa();
        empresa.setCnpj(empresaDTO.getCnpj());
        return cadastroService.cadastrarEmpresa(empresa);

    }

}
