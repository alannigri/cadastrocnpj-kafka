package br.com.empresas.logger;

import br.com.empresas.cadastro.models.EmpresaRetorno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    @Autowired
    private Writer writer;

    @KafkaListener(topics = "spec3-alan-nigri-3", groupId = "Nigri-3")
    public void receber(@Payload EmpresaRetorno empresaRetorno) {
        writer.generateCsvFile("/home/a2w/workspace-espec/Bureau-Kafka/empresas.csv", empresaRetorno);
        System.out.println("recebido!!");
    }
}