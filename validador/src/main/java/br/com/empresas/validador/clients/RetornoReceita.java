package br.com.empresas.validador.clients;

public class RetornoReceita {
    private String status;
    private double capital_social;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getCapital_social() {
        return capital_social;
    }

    public void setCapital_social(double capital_social) {
        this.capital_social = capital_social;
    }
}
